# Demo git B1

Demo de Git pour les B1 2025

## Avant

Se connecter à gitlab.com
Générer une paire de clé SSH

```bash
ssh-keygen -b 4096 -t rsa
```

Copier / coller le ".pub" dans gitlab.com

## Creer un projet

Avec un README.md

## Enregistrer des modifications

```bash
git add .     # Surveiller tout les fichiers
git commit -m "commentaire utile"  # Eregistrer les modifs sur le dépot local"
git push      # Remonter les modifs sur le serveur
```

## Synchroniser avec le serveur / DL le code

```bash
git pull
```
Est-ce qu'il faut faire ça ?
